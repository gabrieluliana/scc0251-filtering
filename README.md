# scc0251-filtering

USP - Universidade de São Paulo

1st semester - 2020
Image Processing - SCC0251

Assignment 2 - Image Enhancement and Filtering

Author:

*  Gabriel Citroni Uliana - NUSP: 9799367

Folders and files:

* Python code contains the Python code used for run.codes submission
* Images contains images used in the demos
* Notebook with Demo is a notebook exemplifying functions developed and submitted
